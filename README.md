## TweaksManager
### What's this?
This simple nearly 200 line bash script will help you manage your iOS Tweaks

### Prerequisites
Jailbroken iPhone with running SSH server or terminal emulator (NewTerm I guess)

### Getting Started
Remember to work on the superuser account - TweakManager only works on that
```sh
$ git clone https://gitlab.com/sech1p/tweakmanager.git
$ cd tweakmanager
$ chmod +x TweakManager
$ ./TweakManager (if doesn't work, use instead: bash TweakManager)
```

### Available commands
```
./TweakManager - launches TweakManager in interactive mode
./TweakManager (TweakName) (on/off) [--no-respring-prompt] - modifies the tweak without entering interactive mode (e.g.: ./TweakManager AnotherAmazingTweak on)
./TweakManager list - returns all installed tweaks on the device
./TweakManager respring - resprings the device
./TweakManager help - returns TweakManager help
```
`--no-respring-prompt` flag can be helpful in scripts where you modify many tweaks in a row

### TODO
- [ ] Make TweakManager frontend using `dialog` (unfortunately, `dialog` has problem with working under NewTerm emulator)
- [ ] Make code less ugly
- [ ] More functions(?)

### License
TweaksManager is licensed under [GNU General Public License](LICENSE)
